﻿namespace Demineur.Enums
{
    /// <summary>
    ///  Game levels
    /// </summary>
    public enum GameLevel
    {
        Beginner, // 0
        Intermediate, // 1
        Expert // 2
    }
}
