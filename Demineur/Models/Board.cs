﻿using Demineur.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Activation;
using System.Text;
using System.Threading.Tasks;

namespace Demineur.Models
{
    public class Board
    {
        #region Constructeurs

        public Board(GameLevel level)
        {
            switch(level)
            {
                case GameLevel.Beginner:
                    Rows = Columns = MaxMines = 10;
                    break;

                case GameLevel.Intermediate:
                    Rows = Columns = 16;
                    MaxMines = 40;
                    break;

                case GameLevel.Expert:
                    Rows = 16;
                    Columns = 30;
                    MaxMines = 99;
                    break;

                default:
                    break;
            }

            // Initialize board
            InitializeCells();

            // Set mines
            SetMines();

            // Debug
            DebugBoard();
        }

        #endregion


        #region Méthodes

        private void InitializeCells()
        {
            // Initailize the board array
            cells = new BoardCell[Rows, Columns];

            // define all celles in the board arrray
            // we have a 20 board so we need to 
            for(int i = 0; i < Rows; i++)
            {

                for (int j = 0; j < Columns; j++)
                {
                    var cell = new BoardCell();

                    cells[i, j] = cell;

                }
            }
        }
        
        private void SetMines()
        {
            var random = new Random();
            for (int compteur = 0; compteur < MaxMines; compteur++)
            {
                var ligne = random.Next(0, Rows); // tire une ligne au hasard
                var colonne = random.Next(0, Columns); // tire une colonne au hasard
                cells[ligne, colonne].HasMine = true;
            }
        }

        private void DebugBoard()
        {
            for (int i = 0; i < Rows; i++)
            {

                for (int j = 0; j < Columns; j++)
                {
                    if(cells[i, j].HasMine == false)
                    {
                        Console.Write(" 0 ");
                    }
                    else
                    {
                        Console.Write(" 1 ");
                    }

                }
                Console.WriteLine();

            }
        }

        #endregion


        #region Propriétés

        public int Columns { get; set; }
        public int Rows { get; set; }
        public int MaxMines { get; set; }

        /// <summary>
        /// 2 dimensions lignes puis colonnes (y,x)
        /// </summary>
        public BoardCell[,] cells { get; set; }

        #endregion
    }

}
