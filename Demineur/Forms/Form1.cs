﻿using Demineur.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demineur
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void MenuBeginner_Click(object sender, EventArgs e)
        {
            var board = new Board(Enums.GameLevel.Beginner);
            boardView.InitializeBoardView(board);
        }

        private void MenuIntermediate_Click(object sender, EventArgs e)
        {
            var board = new Board(Enums.GameLevel.Intermediate);
            boardView.InitializeBoardView(board);
        }

        private void MenuExpert_Click(object sender, EventArgs e)
        {
            var board = new Board(Enums.GameLevel.Expert);
            boardView.InitializeBoardView(board);
        }
    }
}
