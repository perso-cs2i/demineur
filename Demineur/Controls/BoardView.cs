﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Demineur.Models;

namespace Demineur.Controls
{
    public partial class BoardView : UserControl
    {
        public BoardView()
        {
            InitializeComponent();
        }

       
        public void InitializeBoardView(Board board)
        {
            var colPixels = 0;
            var rowPixels = 0;

            // clear
            Controls.Clear();

            for(int row = 0; row < board.Rows; row++)
            {
                for (int col = 0; col < board.Columns; col++)
                {
                    var myPictureBox = new PictureBox();
                    myPictureBox.Image = Properties.Resources.standard;

                    // PictureBox size
                    myPictureBox.Width = CellSize;
                    myPictureBox.Height = CellSize;

                    // coordinates
                    myPictureBox.Left = colPixels;
                    myPictureBox.Top = rowPixels;

                    // Add PictureBox to this UserControl (the BoardView)
                    this.Controls.Add(myPictureBox);

                    colPixels += CellSize;
                }

                // New row
                colPixels = 0;
                rowPixels += CellSize;

                Width = board.Columns * CellSize;
                Height = board.Rows * CellSize;
            }
        }

        public void BoardView_Load()
        {

        }
    }
}
