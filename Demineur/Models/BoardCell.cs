﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demineur.Models
{
    /// <summary>
    /// Board cell
    /// (the class that manages a celle state)
    /// </summary>
    public class BoardCell
    {
        #region Constructeurs
        
        /// <summary>
        /// Constructor of board cell
        /// </summary>
        public BoardCell()
        {
            HasMine = false;
            HasFlag = false;
            MinesAround = -1;
        }

        #endregion

        #region Propriétés

        /// <summary>
        /// Is there a mine on the cell ?
        /// </summary>
        public bool HasMine { get; set; }

        /// <summary>
        /// Is there a flag on the cell ?
        /// </summary>
        public bool HasFlag { get; set; }

        /// <summary>
        /// Number of mines around this cell
        /// If this value is -1, the cell hasn't been explored (clicked)
        /// </summary>
        public int MinesAround { get; set; }

        #endregion
    }
}
